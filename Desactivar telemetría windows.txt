Desactivar telemetría windows:
1. Abre regedit
2. Escribe Equipo\HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\DataCollection en la zona superior
3. Click derecho > Nuevo > Valor de DWORD (32-bit) y escribir: Allow Telemetry
4. Asegúrate de que el valor es 0
5. Reinicia el ordenador